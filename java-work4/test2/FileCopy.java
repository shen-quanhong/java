/* 实现将文件复制于一个文件下
package test2;

import java.util.*;
import java.io.*;

public class FileCopy{
	public static void main(String args[]){
		File dirSource = new File("./files");
		File dirCopy = new File("./filescopy");
		if(!dirCopy.exists()){
			dirCopy.mkdir();
		}
		File [] file = dirSource.listFiles();
		byte [] a = new byte [1010];
		try{
			InputStream in;
			OutputStream out;
			for(int i = 0;i<2;++i){
				if(!file[i].isDirectory()){
					in = new FileInputStream(file[i]);
					out = new FileOutputStream(new File("./filescopy",file[i].getName()));
					int n = 0;
					while((n = in.read(a,0,1000))!=-1)
						out.write(a,0,n);
					out.flush();
					in.close();
					out.close();
				}
				else{
					File [] secfile = file[i].listFiles();
					for(int j = 0;j<2;++j){
						in = new FileInputStream(secfile[j]);
						out = new FileOutputStream(new File("./filescopy",secfile[j].getName()));
						int n = 0;
						while((n = in.read(a,0,1000))!=-1)
							out.write(a,0,n);
						out.flush();
						in.close();
						out.close();
					}
				}
			}
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
	}
}
*/

package test2;

import java.util.*;
import java.io.*;

class NewFile{
	public void copy(String parent,String newroad){
		try{
			File dirSource = new File(parent);
			String [] f = dirSource.list();
			for(String name : f){
				File source = new File(parent,name);
				if(source.isDirectory()){
					File file = new File(newroad+"/"+name);
					file.mkdir();
					copy(parent+"/"+name,newroad+"/"+name);
				}
				else{
					File file = new File(newroad,name);
					byte [] b = new byte [1010];
					InputStream in = new FileInputStream(source);
					OutputStream out = new FileOutputStream(file);
					int n = 0;
					while((n = in.read(b,0,1000))!=-1)
						out.write(b,0,n);
					out.flush();
					in.close();
					out.close();
				}
			}
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
	}
}
public class FileCopy{
	public static void main(String args[]){
		File dirCopy = new File("./filescopy");
		if(!dirCopy.exists()){
			dirCopy.mkdir();
		}
		NewFile file = new NewFile();
		file.copy("./files","./filescopy");
	}
}
