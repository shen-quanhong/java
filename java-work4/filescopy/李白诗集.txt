将进酒            李白

君不见，黄河之水天上来，奔流到海不复回。
君不见，高堂明镜悲白发，朝如青丝暮成雪。
人生得意须尽欢，莫使金樽空对月。
天生我材必有用，千金散尽还复来。
烹羊宰牛且为乐，会须一饮三百杯。
岑夫子，丹丘生，将进酒，君莫停。
与君歌一曲，请君为我侧耳听。
钟鼓馔玉不足贵，但愿长醉不愿醒。
古来圣贤皆寂寞，惟有饮者留其名。
陈王昔时宴平乐，斗酒十千恣欢谑。
主人何为言少钱，径须沽取对君酌。
五花马，千金裘，呼儿将出换美酒，
与尔同销万古愁。

五古·咏苎萝山    李白

西施越溪女，出自苎萝山。
秀色掩今古，荷花羞玉颜。
浣纱弄碧水，自与清波闲。
皓齿信难开，沉吟碧云间。
勾践徵绝艳，扬蛾入吴关。
提携馆娃宫，杳渺讵可攀。
一破夫差国，千秋竟不还。
