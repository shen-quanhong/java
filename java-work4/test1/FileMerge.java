package test1;

import java.util.*;
import java.io.*;

public class FileMerge{
	public static void main(String args[]){
		//File [] fSource = new File [2];
		File fTarget = new File("./files","李白诗集.txt");
		File dirFile = new File("./files/poem");
		File [] fSource = dirFile.listFiles();
		try{
			//String fileEncode = System.getProperty("file.encoding");
			Writer out = new FileWriter(fTarget);
			BufferedWriter bufferWrite = new BufferedWriter(out);
			Reader in = new FileReader(fSource[1]);
			BufferedReader bufferRead = new BufferedReader(in);
			String str;
			while((str = bufferRead.readLine())!=null){
				bufferWrite.write(str);
				bufferWrite.newLine();
			}
			bufferWrite.newLine();
			in = new FileReader(fSource[0]);
			bufferRead = new BufferedReader(in);
			while((str = bufferRead.readLine())!=null){
				bufferWrite.write(str);
				bufferWrite.newLine();
			}
			//out.write( new String(str.getBytes( "UTF-8"), fileEncode)); 
			bufferWrite.close();
			out.close();
			bufferRead.close();
			in.close();
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
	}
}
