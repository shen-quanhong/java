package test3;

import java.io.*;

public class Student implements Serializable{
	private int id;
	private String name;
	private int age;
	private School school;

	public Student(){}
	public Student(int id,String name,int age,School school){
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.school = school;
	}
	public String toString(){
		return "Student [id="+id+", name="+name+", age="+age+", school="+school.toString()+"]";
	}
}
