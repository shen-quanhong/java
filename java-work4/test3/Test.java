package test3;

import java.util.*;
import java.io.*;

public class Test{
	public static void main(String args[]){
		int id,age;
		String name,school;
		String str;
		Scanner reader = new Scanner(System.in);
		id = reader.nextInt();
		name = reader.next();
		age = reader.nextInt();
		school = reader.next();
		Student s1 = new Student(id,name,age,new School(school));
		id = reader.nextInt();
		name = reader.next();
		age = reader.nextInt();
		school = reader.next();
		Student s2 = new Student(id,name,age,new School(school));
		File file = new File("./test3","Student.txt");
		try{
			OutputStream out = new FileOutputStream(file);
			ObjectOutputStream output = new ObjectOutputStream(out);
			output.writeObject(s1);
			output.writeObject(s2);
			InputStream in = new FileInputStream(file);
			ObjectInputStream input = new ObjectInputStream(in);
			Student t1 = (Student)input.readObject();
			Student t2 = (Student)input.readObject();
			System.out.println(t1.toString());
			System.out.println(t2.toString());
			input.close();
			output.close();
			in.close();
			out.close();
		}
		catch(ClassNotFoundException event){
			System.out.println("cannot find Object");
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
	}
}
