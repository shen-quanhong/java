package test1;

public class Circle extends Shape{
	private double radium;
	private final double PI = 3.1415926;
	public Circle(){
		radium = 10;
	}
	public Circle(double r){
		radium = r;
	}
	public double getArea(){
		return PI*radium*radium;
	}
}
