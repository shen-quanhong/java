package test1;

public class Cylinder<T extends Shape> {
	private T s1;
	private double height;
	public Cylinder(T s,double h){
		this.s1 = s;
		height = h;
	}
	public double getVolume(){
		return s1.getArea()*height;
	}
}
