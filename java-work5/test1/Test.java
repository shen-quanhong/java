package test1;

public class Test{
	public static void main(String args[]){
		Rectangle rec = new Rectangle();
		Circle cir = new Circle();
		Cylinder<Rectangle> c1 = new Cylinder<Rectangle>(rec,100);
		Cylinder<Circle> c2 = new Cylinder<Circle>(cir,1000);
		System.out.println("Rectangle体积："+c1.getVolume());
		System.out.println("Circle体积："+c2.getVolume());
	}
}
