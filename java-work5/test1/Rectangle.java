package test1;

public class Rectangle extends Shape{
	private double length,width;
	public Rectangle(){
		length = 20;
		width = 10;
	}
	public Rectangle(double a,double b){
		length = a;
		width = b;
	}
	public double getArea(){
		return length * width;
	}
}
