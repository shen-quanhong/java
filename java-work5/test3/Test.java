package test3;

import java.util.Map.*;
import java.util.*;

public class Test{
	public static void main(String args[]){
		TreeMap<Student,String> MapStu = new TreeMap<Student,String>(new MyComparator());
		MapStu.put(new Student("李明","1001","男",19),"上海");
		MapStu.put(new Student("张帆","2321","女",20),"广州");
		MapStu.put(new Student("胡清","1234","女",20),"上海");
		MapStu.put(new Student("宋粉红","4323","男",18),"青岛");
		MapStu.put(new Student("唐休","1839","女",23),"成都");
		MapStu.put(new Student("陶宏","5234","男",20),"西安");
		MapStu.put(new Student("吴洪","1004","女",19),"成都");
		for(Entry<Student,String> entry:MapStu.entrySet())
			System.out.println(entry.getKey()+" "+entry.getValue());
		System.out.print("----请输入需要删除同学的学号:");
		Scanner reader = new Scanner(System.in);
		String nu = reader.next();
		for(Iterator<Entry<Student,String> > iter = MapStu.entrySet().iterator();iter.hasNext();){
			Entry<Student,String> entry = iter.next();
			Student key = entry.getKey();
			String val = entry.getValue();
			if(key.getNum().equals(nu)){
				System.out.println(key+" "+val);
				MapStu.remove(key);
				break;
			}
		}
		TreeSet<String> city = new TreeSet<String>();
		System.out.println("----删除后的同学信息：");
		for(Entry<Student,String> entry:MapStu.entrySet()){
			if(city.add(entry.getValue())){
				System.out.println(entry.getValue()+":");
				for(Entry<Student,String> trans:MapStu.entrySet()){
					if(entry.getValue().equals(trans.getValue()))
						System.out.println(trans.getKey()+" "+trans.getValue());
				}
			}
		}
	}
}
