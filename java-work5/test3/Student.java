package test3;

public class Student{
	private String name;
	private String number;
	private String sex;
	private int age;
	public Student(){}
	public Student(String name,String number,String sex,int age){
		this.name = name;
		this.number = number;
		this.sex = sex;
		this.age = age;
	}
	public Student(Student s){
		this.name = s.name;
		this.number = s.number;
		this.sex = s.sex;
		this.age = s.age;
	}
	public String getNum(){
		return number;
	}
	public String toString(){
		return name+" "+number+" "+sex+" "+age;
	}
}
