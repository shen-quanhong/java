package test3;

import java.util.Comparator;

public class MyComparator implements Comparator<Student>{
	public int compare(Student a,Student b){
		return a.getNum().compareTo(b.getNum());
	}
}
