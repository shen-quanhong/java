package test2;

import java.util.Comparator;

public class MyComparator implements Comparator<City>{
	public int compare(City a,City b){
		return b.getGDP()-a.getGDP();
	}
}
