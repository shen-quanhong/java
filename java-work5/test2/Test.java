package test2;

import java.util.*;

public class Test {
	public static void main(String args[]){
		City[] city = new City [10];
		city[0] = new City("shanghai",30133,2418,63405);
		city[1] = new City("chengdu",13890,1592,14335);
		city[2] = new City("guangzhou",21500,1404,7434.4);
		city[3] = new City("nanjing",11715,827,6622.45);
		city[4] = new City("changsha",10200,765,11819);
		city[5] = new City("beijing",28000,2171,16410);
		city[6] = new City("chongqin",19530,3372,82400);
		city[7] = new City("shuzhou",17000,1065,8848.42);
		city[8] = new City("ningbo",9850,788,9816);
		city[9] = new City("hangzhou",12556,919,16853.57);
		ArrayList<City> mylist = new ArrayList<City>();
		mylist.add(city[0]);
		mylist.add(city[1]);
		mylist.add(city[2]);
		mylist.add(city[3]);
		mylist.add(city[4]);
		mylist.add(city[5]);
		mylist.add(city[6]);
		mylist.add(city[7]);
		mylist.add(city[8]);
		mylist.add(city[9]);
		Collections.sort(mylist,new MyComparator());
		Iterator<City> iter = mylist.iterator();
		while(iter.hasNext()){
			City c = iter.next();
			c.output();
		}
	}
}
