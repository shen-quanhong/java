package test2;

import java.util.*;
public class City{
	private String name;
	private int GDP,population;
	private double area,pDenity,GDP_capita;
	public City(){}
	public City(String n,int g,int po,double a){
		name = n;
		GDP = g;
		population = po;
		area = a;
		pDenity = (double)population / area;
		GDP_capita = (double)GDP / population;
	}
	int getGDP(){
		return GDP;
	}
	public void output(){
		System.out.println(name+" "+GDP+" "+population+" "+area);
	}
}
