package test2;

public class Function implements Runnable{
	private int part;
	public Function(){
		part = 800;
	}
	public void run(){
		int count = 0;
		while(true){
			String name = Thread.currentThread().getName();
			synchronized(this){
				if(part<=0){
					System.out.println(name+" : "+count);
					return ;
				}
			}
			--part;
			++count;
			try{
				Thread.sleep(10);//static function
			}
			catch(InterruptedException e){}
		}
	}
}
