package test3;
public class Consumer extends Thread{
	GoodsShelf shelf;
	public Consumer(String name){
		super(name);
	}
	public void setShelf(GoodsShelf gs){
		shelf = gs;
	}
	public void run(){
		while(true){
			shelf.remove();
			try{
				Thread.sleep(500);
			}
			catch(InterruptedException e){}
		}
	}
}
