package test3;

public class Test{
	public static void main(String args[]){
		GoodsShelf goodsShelf = new GoodsShelf(100,30);
		Producer producer1 = new Producer("生产者一");
		Producer producer2 = new Producer("生产者二");
		Producer producer3 = new Producer("生产者三");
		Consumer consumer1 = new Consumer("消费者一");
		Consumer consumer2 = new Consumer("消费者二");
		producer1.setShelf(goodsShelf);
		producer2.setShelf(goodsShelf);
		producer3.setShelf(goodsShelf);
		consumer1.setShelf(goodsShelf);
		consumer2.setShelf(goodsShelf);
		producer1.start();
		producer2.start();
		producer3.start();
		consumer1.start();
		consumer2.start();
	}
}
