package test3;

class Product{}

public class GoodsShelf {
	Product[] goods;
	int size,top;
	public GoodsShelf(){
		goods = new Product[105];
		size = 100;
		top = 0;
	}
	public GoodsShelf(int size){
		this.size = size;
		goods = new Product[size+5];
		top = 0;
	}
	public GoodsShelf(int size,int remain){
		this.size = size;
		goods = new Product[size+5];
		top = remain;
		for(int i = 1;i<=top;++i)
			goods[i] = new Product();
	}
	public synchronized void add(Product p){
		while(top == size){
			try{
				System.out.println("货架已满，暂停生产!!!");
				wait();
			}
			catch(InterruptedException e){}
		}
		goods[++top] = p;
		System.out.println(Thread.currentThread().getName()+"生产货物"+top);
		notifyAll();
	}
	public synchronized void remove(){
		while(top == 0){
			try{
				System.out.println("货架已空，暂停销售!!!");
				wait();
			}
			catch(InterruptedException e){}
		}
		System.out.println(Thread.currentThread().getName()+"消耗货物"+top);
		goods[top--] = null;
		notifyAll();
	}
	public String toString(){
		return "当前货架剩余货物数目："+top;
	}
}
