package test3;

public class Producer extends Thread {
	GoodsShelf shelf;
	public Producer(String name){
		super(name);
	}
	public void setShelf(GoodsShelf gs){
		shelf = gs;
	}
	public void run(){
		while(true){
			shelf.add(new Product());
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException e){}
		}
	}
}
