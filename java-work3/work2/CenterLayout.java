package work2;

import java.awt.*;
import javax.swing.*;

public class CenterLayout extends JPanel{
	JLabel l[] = new JLabel[42];
	public CenterLayout(int firstDay,int days){
		setLayout(new GridLayout(7,7));
		JButton b[] = new JButton[7];
		b[0] = new JButton("日");
		b[1] = new JButton("一");
		b[2] = new JButton("二");
		b[3] = new JButton("三");
		b[4] = new JButton("四");
		b[5] = new JButton("五");
		b[6] = new JButton("六");
		for(int i = 0;i<7;++i){
			b[i].setSize(90,20);
			add(b[i]);
		}
		for(int i = 0;i<42;++i){
			l[i] = new JLabel("",JLabel.CENTER);
			//l[i] = setAlignment(JLabel.CENTER);
			l[i].setSize(40,20);
			add(l[i]);
		}
		output(firstDay,days);
	}
	public void output(int firstDay,int days){
		String num;
		int m = 1;
		for(int i = 0;i<42;++i){
			l[i].setText("");
			if((i<firstDay-1)||(m>days)) continue;
			num = String.valueOf(m);
			l[i].setText(num);
			++m;
		}
	}
}
