package work2;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Calendar;

public class MyListener implements ActionListener {
	WindowPanel view;
	public MyListener(WindowPanel w){
		view = w;
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == view.north.b1){
			view.month--;
			if(view.month == -1){
				view.year--;
				view.month = 11;
			}
			view.calendar.set(view.year,view.month,1);//设置当前年月
		}
		if(e.getSource() == view.north.b2){
			view.month++;
			if(view.month == 12){
				view.year++;
				view.month = 0;
			}
			view.calendar.set(view.year,view.month,1);
		}
		view.renew(view.calendar);
		view.south.init(view.year,view.month);
		view.center.output(view.firstDay,view.days);
	}
}
