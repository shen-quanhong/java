package work2;

import java.awt.*;
import javax.swing.*;

public class SouthLayout extends JPanel {
	String s;
	JLabel la;
	public SouthLayout(int year,int month){
		setLayout(new FlowLayout());
		la = new JLabel("");
		init(year,month);
		add(la);
	}
	public void init(int year,int month){
		String y = String.valueOf(year);
		String m = String.valueOf(month+1);
		s = "日历："+y+"年"+m+"月";
		la.setText(s);
	}
}
