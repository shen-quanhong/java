package work2;

import java.awt.*;
import javax.swing.*;
import java.util.Calendar;
import java.awt.event.*;

public class WindowPanel extends JFrame {
	public MyListener police;
	public CenterLayout center;
	public NorthLayout north;
	public SouthLayout south;
	public int month;//当前月
	public int year;//当前年
	public int firstDay;//本月第一天
	public int days;//本月天数
	public Calendar calendar;
	public void renew(Calendar cal){
		calendar = cal;
		month = calendar.get(Calendar.MONTH);
		year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.DAY_OF_MONTH,1);//设置今天为本月第一天
		firstDay = calendar.get(Calendar.DAY_OF_WEEK);
		calendar.roll(Calendar.DAY_OF_MONTH,false);//前进一天，到最后一天
		days = calendar.get(Calendar.DAY_OF_MONTH);
	}
	public WindowPanel(Calendar cal){
		renew(cal);
		init();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		center = new CenterLayout(firstDay,days);
		north = new NorthLayout();
		south = new SouthLayout(year,month);
		setLayout(new BorderLayout());
		add(center,BorderLayout.CENTER);
		add(north,BorderLayout.NORTH);
		add(south,BorderLayout.SOUTH);

		police = new MyListener(this);
		//addActionListener(police);
		north.b1.addActionListener(police);
		north.b2.addActionListener(police);
	}
}
