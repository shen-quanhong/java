package work3;

import java.awt.*;
import javax.swing.*;

public class CenterLayout extends JPanel {
	JButton b0;
	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	JButton b5;
	JButton b6;
	JButton b7;
	JButton b8;
	JButton b9;
	JButton op1;
	JButton op2;
	JButton op3;
	JButton op4;
	JButton op5;
	JButton op6;
	public CenterLayout(){
		JButton b0 = new JButton("0");
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		JButton op1 = new JButton("+");
		JButton op2 = new JButton("-");
		JButton op3 = new JButton("*");
		JButton op4 = new JButton("/");
		JButton op5 = new JButton("c");
		JButton op6 = new JButton("=");
		setLayout(new GridLayout(4,4));
		
		add(b1);
		add(b2);
		add(b3);
		add(op1);
		add(b4);
		add(b5);
		add(b6);
		add(op2);
		add(b7);
		add(b8);
		add(b9);
		add(op3);
		add(op5);
		add(b0);
		add(op6);
		add(op4);
	}
}
