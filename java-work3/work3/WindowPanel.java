package work3;

import java.awt.*;
import javax.swing.*;

public class WindowPanel extends JFrame {
	public WindowPanel(){
		setLayout(new BorderLayout());
		center = new CenterLayout();
		north = new NorthLayout();
		add(center,BorderLayout.CENTER);
		add(north,BorderLayout.NORTH);
	}
}
