package work1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class WindowPanel extends JFrame{
	public JPanel win;
	public JTextField Tname,Temail;//TextField
	public JPasswordField Tcode,Tconfirm;//Password
	public JLabel Lname,Lcode,Lconfirm,Lemail;//Label
	public JLabel a1,a2,a3,a4;//Assert
	public JButton b1,b2;//Button
	public Listener eventListener;
	public WindowPanel(){
		init();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		JPanel win =new JPanel();
		win.setLayout(null);
		Tname = new JTextField();
		Tcode = new JPasswordField();
		Tconfirm = new JPasswordField();
		Temail = new JTextField();

		Tname.setBounds(250,50,250,30);
		Tcode.setBounds(250,110,250,30);
		Tconfirm.setBounds(250,170,250,30);
		Temail.setBounds(250,230,250,30);

		//Tcode.setEchoChar('·');
		//Tconfirm.setEchoChar('·');

		Lname = new JLabel("用户名：");
		Lcode = new JLabel("密码：");
		Lconfirm = new JLabel("确认密码：");
		Lemail = new JLabel("邮箱：");
		a1 = new JLabel("");
		a2 = new JLabel("");
		a3 = new JLabel("");
		a4 = new JLabel("");

		Lname.setBounds(100,50,80,30);
		Lcode.setBounds(100,110,80,30);
		Lconfirm.setBounds(100,170,80,30);
		Lemail.setBounds(100,230,80,30);
		a1.setBounds(260,82,300,30);
		a2.setBounds(260,142,300,30);
		a3.setBounds(260,202,300,30);
		a4.setBounds(260,262,300,30);

		a1.setForeground(Color.red);
		a2.setForeground(Color.red);
		a3.setForeground(Color.red);
		a4.setForeground(Color.red);

		b1 = new JButton("提交");
		b2 = new JButton("取消");

		b1.setBounds(160,290,100,30);
		b2.setBounds(330,290,100,30);

		eventListener = new Listener(this);

		Tname.addFocusListener(eventListener);
		Tcode.addFocusListener(eventListener);
		Tconfirm.addFocusListener(eventListener);
		Temail.addFocusListener(eventListener);
		b1.addActionListener(eventListener);
		b2.addActionListener(eventListener);

		win.add(Tname);
		win.add(Tcode);
		win.add(Tconfirm);
		win.add(Temail);
		win.add(Lname);
		win.add(Lcode);
		win.add(Lconfirm);
		win.add(Lemail);
		win.add(a1);
		win.add(a2);
		win.add(a3);
		win.add(a4);
		win.add(b1);
		win.add(b2);

		getContentPane().add(win);
	}
}
