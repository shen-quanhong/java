package work1;

import java.awt.event.*;
import java.awt.Window.*;
import javax.swing.*;

public class Listener implements FocusListener,ActionListener{
	WindowPanel view;
	int s1,s2,s3,s4;
	String name,code,confirm,email;
	String UserRegex = "\\S{6,20}";
	String CodeRegex = "[[a-zA-Z]|\\p{Punct}|\\p{Digit}]{8,20}";
	String EmailRegex = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
	public Listener(WindowPanel v){
		this.view = v;
		s1 = s2 = s3 = s4 = 0;
	}
	public void focusGained(FocusEvent e){
		if(view.Tname.isFocusOwner()==true)
			s1 = 0;
		else if(view.Tcode.isFocusOwner()==true){
			s2 = 0;
		}
		else if(view.Tconfirm.isFocusOwner()==true){
			s3 = 0;
		}
		else if(view.Temail.isFocusOwner()==true)
			s4 = 0;
	}
	public void focusLost(FocusEvent e){
		if(view.Tname.isFocusOwner() == false && view.Tname.getSelectionStart() != 0 && s1 == 0){
			name = view.Tname.getText();
			if(name.matches(UserRegex) == false){
				view.a1.setText("用户名必须是不含空字符的6-20位字符串");
				s1 = -1;
			}
			else{
				view.a1.setText("");
				s1 = 1;
			}
		}
		if(view.Tcode.isFocusOwner() == false && view.Tcode.getSelectionStart() != 0 && s2 == 0){
			char tmp[] = new char[100];
			tmp = view.Tcode.getPassword();
			code = String.valueOf(tmp);
			if(code.matches(CodeRegex)==false){
				view.a2.setText("密码必须是8-20位的可含标点符号或单词字符的字符串");
				s2 = -1;
			}
			else{
				view.a2.setText("");
				s2 = 1;
			}
		}
		if(view.Tconfirm.isFocusOwner()==false&&view.Tconfirm.getSelectionStart()!=0&&s3==0&&s2!=0){
			char tmp[] = new char[100];
			tmp = view.Tconfirm.getPassword();
			confirm = String.valueOf(tmp);
			if(code.equals(confirm)==false){
				view.a3.setText("确认密码必须于密码相同");
				s3 = -1;
			}
			else{
				view.a3.setText("");
				s3 = 1;
			}
		}
		if(view.Temail.isFocusOwner()==false&&view.Temail.getSelectionStart()!=0&&s4==0){
			email = view.Temail.getText();
			if(email.matches(EmailRegex)==false){
				view.a4.setText("请输入合法的邮箱地址!");
				s4 = -1;
			}
			else{
				view.a4.setText("");
				s4 = 1;
			}
		}
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == view.b1&&s1==1&&s2==1&&s3==1&&s4==1)
			JOptionPane.showMessageDialog(view,"注册成功","注册提示",JOptionPane.INFORMATION_MESSAGE);
		if(e.getSource() == view.b2)
			view.dispose();
	}
}
