package work1;

public class Country implements Comparable<Country> {
	private String name;
	private int goal;
	private int silver;
	private int copper;
	private int total;
	public Country() {
		goal = 0;
		silver = 0;
		copper = 0;
		total = 0;
		name = "无";
	}
	public Country(String n,int g,int s,int c) {
		name = n;
		goal = g;
		silver = s;
		copper = c;
		total = goal + silver + copper;
	}
	public void Set(String n,int g,int s,int c) {
		name = n;
		goal = g;
		silver = s;
		copper = c;
		total = goal + silver + copper;
	}
	public void Output(int i) {
		System.out.println(i+"  "+name+"   "+goal+"   "+silver+"   "+copper+"   "+total);
	}
	public int getGoal(){
		return goal;
	}
	public int getSilver(){
		return silver;
	}
	public int getCopper(){
		return copper;
	}
	public int compareTo(Country c) {
		if(this.getGoal() <= c.getGoal()){
			if(this.getGoal() == c.getGoal()){
				if(this.getSilver() <= c.getSilver()){
					if(this.getSilver()==c.getSilver()){
						if(this.getCopper()<=c.getCopper()){
							if(this.getCopper()==c.getCopper())
								return 0;
							return 1;
						}
						return -1;
					}
					return 1;
				}
				return -1;
			}		
			return 1;
		}
		return -1;
	}
}
