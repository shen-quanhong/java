package work3;

public class BankAccount{
	private int amount;
	public BankAccount(){
		amount = 2000;
	}
	public void deposite(int dAmount) throws NegativeAmountException{
		if(dAmount < 0)
			throw new NegativeAmountException(dAmount);
		amount += dAmount;
		System.out.println("存入 "+dAmount+",当前余额："+amount);
	}
	public void withdraw(int dAmount) throws NegativeAmountException,InsufficientFundsException{
		if(dAmount < 0)
			throw new NegativeAmountException(dAmount);
		if(dAmount > amount)
			throw new InsufficientFundsException();
		amount -= dAmount;
		System.out.println("取出 "+dAmount+",当前余额："+amount);
	}
	public void showBalance(){
		System.out.println("账户当前余额："+amount);
	}
}
