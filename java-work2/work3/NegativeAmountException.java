package work3;

public class NegativeAmountException extends Exception{
	String message;
	public NegativeAmountException(int dAmount){
		message = "数据非法: "+dAmount;
	}
	public String getMessage(){
		return message;
	}
}
