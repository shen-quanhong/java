package work3;

import java.util.Scanner;

public class ExceptionTest{
	public static void main(String str[]){
		BankAccount account = new BankAccount();
		Scanner reader = new Scanner(System.in);
		account.showBalance();
		while(true){
			System.out.print("输入操作及金额:");
		//	Scanner reader = new Scanner(System.in);
			String op = reader.next();
			if(op.equals("exit")){
				break;
			}
			try{
				if(op.equals("save"))
					account.deposite(reader.nextInt());
			}
			catch(NegativeAmountException n){
				System.out.print("存入");
				System.out.println(n.getMessage());
			}
			try{
				if(op.equals("draw"))
					account.withdraw(reader.nextInt());
			}
			catch(NegativeAmountException n){
				System.out.print("取出");
				System.out.println(n.getMessage());
			}
			catch(InsufficientFundsException i){
				System.out.println(i.getMessage());
			}
		}
	}
}

