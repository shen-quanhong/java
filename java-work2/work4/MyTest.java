package work4;

import java.util.Scanner;
import java.util.regex.*;

public class MyTest{
	public static void main(String args[]){
		Scanner reader = new Scanner(System.in);
		String all = reader.nextLine();
		String regex = "[\\d-]+";
		String name[] = all.split(regex);
		String[] number = new String[name.length];
		int k = 0;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(all);
		while(matcher.find()){
			number[k] = matcher.group();
			++k;
		}
		Person p[] = new Person[name.length];
		for(int i = 0;i<name.length;++i){
			p[i] = new Person(name[i],number[i]);
			p[i].output();
		}
	}
}
		
