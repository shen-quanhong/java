package work2;

import java.util.Calendar;

public class MyCalendar{
	private int month;//当前月
	private int year;//当前年
	private int firstDay;//本月第一天
	private int days;//本月天数
	private Calendar calendar;
	public MyCalendar(Calendar cal){
		calendar = cal;
		month = calendar.get(Calendar.MONTH);
		year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.DAY_OF_MONTH,1);//设置今天为本月第一天
		firstDay = calendar.get(Calendar.DAY_OF_WEEK);
		calendar.roll(Calendar.DAY_OF_MONTH,false);//前进一天，到最后一天
		days = calendar.get(Calendar.DAY_OF_MONTH);
	}
	public void initial(Calendar cal){
		calendar = cal;
		month = calendar.get(Calendar.MONTH);
		year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.DAY_OF_MONTH,1);
		firstDay = calendar.get(Calendar.DAY_OF_WEEK);
		calendar.roll(Calendar.DAY_OF_MONTH,false);
		days = calendar.get(Calendar.DAY_OF_MONTH);
	}
	public void OutPut(){
		System.out.println(year+"年"+(month+1)+"月");
		System.out.println("\t日\t一\t二\t三\t四\t五\t六");
		for(int i = 1;i<firstDay;++i)
			System.out.print("\t");
		int cnt = 1;
		for(int i = 1;i<=(8-firstDay);++cnt,++i)
			System.out.print("\t"+cnt);
		System.out.println();
		int k = 0;
		for(;cnt<=days;++cnt){
			++k;
			System.out.print("\t"+cnt);
			if(k == 7){
				k = 0;
				System.out.println();
			}
		}
		System.out.println();
	}
	public void setMonth(String str){
		if(str.equals("p")){
			month--;
			if(month == -1){
				year--;
				month = 11;
			}
			calendar.set(year,month,1);//设置当前年月
		}
		else if(str.equals("n")){
			month++;
			if(month == 12){
				year++;
				month = 0;
			}
			calendar.set(year,month,1);
		}
		initial(calendar);//初始化数据
	}
}
