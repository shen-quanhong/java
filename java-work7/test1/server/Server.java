package test1.server;

import java.io.*;
import java.net.*;

public class Server{
	public static void main(String args[]){
		ServerSocket sSocket = null;
		Socket socket = null;
		try{
			sSocket = new ServerSocket(2021);	//设置服务器端口
		}catch(IOException e){
			System.out.println("端口已被占用");
		}
		try{
			socket = sSocket.accept();	//等待连接客户端
			System.out.println(socket.getInetAddress());	//获得远程客户端IP地址
			System.out.println(socket.getPort());	//获取远程客户端端口
			System.out.println(socket.getLocalSocketAddress());	//获得本地服务器IP地址
			System.out.println(socket.getLocalPort());	//获得本地服务器端口
			socket.close();
		}catch (IOException e){
			System.out.println("连接客户端异常");
		}
	}
}
