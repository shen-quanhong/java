package test1.client;

import java.net.*;
import java.io.*;

public class Client{
	public static void main(String args[]){
		Socket socket = new Socket();	//实例化一个客户端套接字
		InetSocketAddress isa = new InetSocketAddress("127.0.0.1",2021);
		try{
			socket.connect(isa);	//通过服务器端主机IP和端口连接到对应服务器
			System.out.println(socket.getInetAddress());//获取远程服务器端IP地址
			System.out.println(socket.getPort());	//获取远程服务器端端口
			System.out.println(socket.getLocalSocketAddress());	//获取本地客户端IP地址
			System.out.println(socket.getLocalPort());	//获取本地客户端端口
			socket.close();
		}catch(IOException e){
			System.out.println("连接服务器异常");
		}
	}
}
