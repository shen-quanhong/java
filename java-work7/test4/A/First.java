package test4.A;

import java.net.*;
import java.util.*;

public class First{
	public static void main(String args[]){
		Scanner scanner = new Scanner(System.in);
		Thread readData;
		ReceiveA receiver = new ReceiveA();
		try{
			readData = new Thread(receiver);
			readData.start();
			byte[] buffer = new byte[1];
			InetAddress address = InetAddress.getByName("127.0.0.1");
			DatagramPacket dataPack = new DatagramPacket(buffer,buffer.length,address,2021);
			DatagramSocket postman = new DatagramSocket();
			System.out.print("输入到B的信息:");
			while(scanner.hasNext()){
				String mess = scanner.nextLine();
				buffer = mess.getBytes();
				if(mess.length()==0)
					System.exit(0);
				buffer = mess.getBytes();
				dataPack.setData(buffer);
				postman.send(dataPack);
				System.out.print("输入到B的信息:");
			}
		}
		catch(Exception exp){
			System.out.println(exp);
		}
	}
}
