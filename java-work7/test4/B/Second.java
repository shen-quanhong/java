package test4.B;

import java.net.*;
import java.util.*;

public class Second{
	public static void main(String args[]){
		Scanner scanner = new Scanner(System.in);
		Thread readData;
		ReceiveB receiver = new ReceiveB();
		try{
			readData = new Thread(receiver);
			readData.start();
			byte[] buffer =new byte[1];
			InetAddress address = InetAddress.getByName("127.0.0.1");
			DatagramPacket dataPack = new DatagramPacket(buffer,buffer.length,address,2022);
			DatagramSocket postman = new DatagramSocket();
			System.out.print("输入到A的信息:");
			while(scanner.hasNext()){
				String mess = scanner.nextLine();
				if(mess.length()==0)
					System.exit(0);
				buffer = mess.getBytes();
				dataPack.setData(buffer);
				postman.send(dataPack);
				System.out.print("输入到A的信息:");
			}
		}
		catch(Exception exp){
			System.out.println(exp);
		}
	}
}
