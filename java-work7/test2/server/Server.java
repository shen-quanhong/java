package test2.server;

import java.net.*;
import java.io.*;
import java.util.*;

public class Server{
	public static void main(String args[]){
		ServerSocket server = null;
		Socket socket = null;
		try{
			server = new ServerSocket(2021);
			server.setSoTimeout(30000);
		}catch(IOException e){
			System.out.println("Port have been using!");
		}
		while(true){
			try{
				socket = server.accept();
				System.out.println("Client IP:"+socket.getInetAddress());
			}
			catch(IOException e){
				System.out.println("client connect overtime!");
				System.exit(0);
			}
			if(socket != null){
				new ServerThread(socket).start();
			}
		}
	}
}

class ServerThread extends Thread{
	Socket socket;
	DataInputStream indata;
	FileInputStream fin;
	DataOutputStream out;
	public ServerThread(Socket t){
		socket = t;
		try{
			indata = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
		}catch(IOException e){}
	}
	public void run(){
		File dirSource =new File("./source");
		try{
			String s = indata.readUTF();
			if(s.equals("Finish")){
				socket.close();
				return ;
			}
			File file = new File(dirSource,s);
			fin = new FileInputStream(file);
			byte b[] = new byte[1024];
			int n = 0;
			while((n=fin.read(b))!=-1)
				out.write(b,0,n);
			out.flush();
			System.out.println("File transport success!");
			fin.close();
			out.close();
			indata.close();
			socket.close();
		}catch(IOException e){
			System.out.println("File transport failure!");
			e.printStackTrace();
		}
	}
}

