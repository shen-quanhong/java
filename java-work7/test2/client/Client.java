package test2.client;

import java.io.*;
import java.util.*;
import java.net.*;

public class Client{
	public static void main(String args[]){
		Socket socket = null;
		Scanner scanner = new Scanner(System.in);
		DataOutputStream outdata;
		while(true){
			try{
				socket = new Socket("127.0.0.1",2021);
				System.out.println("Server IP:"+socket.getInetAddress());
			}catch(IOException e){
				System.out.println("Server connection failure");
				return ;
			}
			String str = scanner.next();
			try{
				outdata = new DataOutputStream(socket.getOutputStream());
				outdata.writeUTF(str);
				if(str.equals("Finish")){
					socket.close();
					System.exit(0);
				}
				new ClientThread(socket,str).start();
			}catch(IOException e){
				System.out.println("File name attach error");
			}
		}
	}
}

class ClientThread extends Thread{
	DataInputStream in;
	FileOutputStream out;
	Socket socket;
	String name;
	public ClientThread(Socket t,String s){
		socket = t;
		try{
			in = new DataInputStream(socket.getInputStream());
			name = s;
		}catch(IOException e){}
	}
	public void run(){
		try{
			File file = new File("./destination",name);
			out = new FileOutputStream(file);
			byte[] b = new byte[1024];
			int n = 0;
			while((n = in.read(b))!=-1)
				out.write(b,0,n);
			out.flush();
			System.out.println("File transport success!");
			in.close();
			out.close();
			socket.close();
		}
		catch(Exception e){
			System.out.println("File transport failure!");
			e.printStackTrace();
		}
	}
}
