package test3.client;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class WindowPanel extends JFrame{
	public JButton con,send;
	public JLabel La,Lb,Lc;
	public JTextField Ta,Tb,Tc;
	public JTextArea text;
	public MyListener police;
	public WindowPanel(){
		init();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		setLayout(null);
		police = new MyListener(this,new Client());
		con = new JButton("连接到服务器");
		send = new JButton("send");
		
		con.setBounds(0,0,600,30);
		send.setBounds(400,35,200,30);

		Ta = new JTextField();
		Tb = new JTextField();
		Tc = new JTextField();

		Ta.setBounds(40,35,80,30);
		Tb.setBounds(160,35,80,30);
		Tc.setBounds(280,35,80,30);

		La = new JLabel("SideA");
		Lb = new JLabel("sideB");
		Lc = new JLabel("sideC");

		La.setBounds(0,35,40,30);
		Lb.setBounds(120,35,40,30);
		Lc.setBounds(240,35,40,30);

		text = new JTextArea();

		text.setBounds(0,100,600,300);

		con.addActionListener(police);
		send.addActionListener(police);

		add(Ta);
		add(Tb);
		add(Tc);
		add(La);
		add(Lb);
		add(Lc);
		add(text);
		add(con);
		add(send);

	}
}
