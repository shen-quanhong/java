package test3.client;

import java.net.*;
import java.io.*;
import test3.*;

public class Client {
	public Socket socket;
	private InetSocketAddress isa;
	public Triangle t;
	public ObjectOutputStream out;
	public ObjectInputStream in;
	public Client(){
		socket = new Socket();
		isa = new InetSocketAddress("127.0.0.1",2021);
	}
	public String connection(){
		try{
			socket.connect(isa);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		}catch(IOException e){
			return "连接服务器异常";
		}
		return "连接服务器成功,当前端口地址:"+socket.getLocalSocketAddress();
	}
	public String send(double a,double b,double c){
		t = new Triangle(a,b,c);
		Triangle copy;
		try{
			out.writeObject(t);
			copy = (Triangle)in.readObject();
			return "服务器返回结果:"+copy.area;
		}catch(IOException e){
			return "客户端数据传输错误";
		}catch(ClassNotFoundException e){
			return "客户端无对象";
		}
	}
}
