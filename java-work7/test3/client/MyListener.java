package test3.client;

import javax.swing.*;
import java.awt.event.*;

public class MyListener implements ActionListener{
	WindowPanel view;
	Client clients;
	double a,b,c;
	String str;
	public MyListener(WindowPanel v,Client c){
		view = v;
		clients = c;
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == view.con){
			str = clients.connection();
			view.text.append(str+"\n");
		}
		if(e.getSource() == view.send){
			try{
				a = Double.parseDouble(view.Ta.getText());
				b = Double.parseDouble(view.Tb.getText());
				c = Double.parseDouble(view.Tc.getText());
				str = clients.send(a,b,c);
				view.text.append(str+"\n");
			}catch(Exception exp){
				view.text.append("请输入数字"+"\n");
			}
		}
	}
}
