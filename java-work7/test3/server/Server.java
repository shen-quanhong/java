package test3.server;

import java.net.*;
import java.io.*;
import test3.*;

public class Server{
	public static void main(String args[]){
		WindowPanel win = new WindowPanel();
		win.setBounds(100,100,600,400);
		win.setTitle("服务器");
		ServerSocket server = null;
		Socket socket = null;
		try{
			server = new ServerSocket(2021,5);
		}catch(IOException e){
			win.text.append("端口已被占用"+"\n");
		}
		while(true){
			try{
				socket = server.accept();
				win.Lab.setText(socket.getLocalSocketAddress().toString());
				if(socket!=null){
					new ServerThread(socket,win).start();

				}
				win.text.append("客户端IP地址:"+socket.getInetAddress()+"\n");
			}
			catch(IOException e){
				win.text.append("客户端连接异常"+"\n");
			}
		}
	}
}

class ServerThread extends Thread{
	public Socket socket;
	public WindowPanel win;
	public ObjectInputStream in;
	public ObjectOutputStream out;
	public ServerThread(Socket t,WindowPanel w){
		socket = t;
		win = w;
		try{
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
		}catch(IOException e){
			win.text.append(getName()+"对象流搭建失败"+"\n");
		}
	}
	public void run(){
		while(true){
			try{
				Triangle t = (Triangle)in.readObject();
				t.calculateArea();
				out.writeObject(t);
			}catch(IOException e){
				win.text.append(getName()+"客户端异常"+"\n");
				return;
			}
			catch(ClassNotFoundException e){
				win.text.append(getName()+"无对象"+"\n");
				return ;
			}
		}
	}
}
