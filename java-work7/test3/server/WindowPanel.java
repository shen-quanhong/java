package test3.server;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class WindowPanel extends JFrame{
	public JLabel Lab;
	public JTextArea text;
	public WindowPanel(){
		init();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		setLayout(null);
		Lab = new JLabel();
		Lab.setBounds(0,0,600,50);
		text = new JTextArea();
		text.setBounds(0,55,600,345);
		add(Lab);
		add(text);
	}
}
