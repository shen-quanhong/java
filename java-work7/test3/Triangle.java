package test3;

import java.io.*;

public class Triangle implements Serializable{
	public double sideA;
	public double sideB;
	public double sideC;
	public String area;
	public Triangle(double a,double b,double c){
		this.sideA = a;
		this.sideB = b;
		this.sideC = c;
	}
	public boolean isLegal(){
		if((sideA+sideB>sideC)&&(sideA+sideC>sideB)&&(sideB+sideC>sideA)){
			return true;
		}
		else
			return false;
	}
	public String calculateArea(){
		if(!isLegal()){
			area = "输入数据不合法，不能构成三角形";
			return area;
		}
		double p = (sideA+sideB+sideC)/2.0;
		area = ""+Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
		return area;
	}
}
